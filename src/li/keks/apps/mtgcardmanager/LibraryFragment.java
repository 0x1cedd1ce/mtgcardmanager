package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.app.LoaderManager;

public class LibraryFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Card>>, OnItemLongClickListener, OnItemClickListener,
														 ActionMode.Callback, OnCreateContextMenuListener, OnClickListener {

    public static final String ARG_CARDS = "cards";
    public static final String ARG_SECTION_NUMBER = "section";
    
    private static class CardLoader extends AsyncTaskLoader<List<Card>> {
        
        private boolean allCards;
        private DatabaseAdapter dbAdapter;

        public CardLoader(Context context, boolean allCards, DatabaseAdapter dbAdapter) {
            super(context);
            this.allCards = allCards;
            this.dbAdapter = dbAdapter;
        }
        
        @Override
        public List<Card> loadInBackground() {
            List<Card> cards = new ArrayList<Card>();
            if(allCards) {
                Log.i("Loader", "get all cards");
                cards.addAll(dbAdapter.getAllCards());
                if(cards.size() == 0) {
                    Log.i("Loader", "parsing json");
                    JSONAdapter adapter = new JSONAdapter(getContext());
                    cards.addAll(adapter.parseCards());
                    Log.i("Loader", "add cards to db");
                    dbAdapter.addAllCards(cards);
                }
            } else {
                Log.i("Loader", "get collection");
                cards.addAll(dbAdapter.getAllCardsFromCollection());
            }
            return cards;
        }
        
        @Override
        public void deliverResult(List<Card> cards) {
            Log.i("Loader", "deliverResult");
            super.deliverResult(cards);
        }
        
        @Override
        protected void onStartLoading() {
            Log.i("Loader", "onStartLoading");
            forceLoad();
        }
        
    }
    
    private DatabaseAdapter dbAdapter = null;
    private CardAdapter adapter = null;
	private List<Card> mSelectedCards = new ArrayList<Card>();
	private List<View> mSelectedViews = new ArrayList<View>();
	private boolean allCards;
	private ArrayAdapter<CardFilter> filterAdapter = null;
   
    public static LibraryFragment newInstance(boolean allCards, int sectionNumber) {
        final LibraryFragment fragment = new LibraryFragment();
        final Bundle args = new Bundle();
        args.putBoolean(ARG_CARDS, allCards);
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }
    
    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        if(filterAdapter == null) {
            filterAdapter = new ArrayAdapter<CardFilter>(getActivity(), R.layout.filter_item, R.id.text1);
        }
        if(dbAdapter == null) {
            dbAdapter = new DatabaseAdapter(getActivity());
        }
        final View rootView = inflater.inflate(R.layout.fragment_library,
                container, false);
        rootView.setOnCreateContextMenuListener(this);
        final ListView listView = (ListView) rootView.findViewById(R.id.listViewCards);
        List<Card> cards = new ArrayList<Card>();
        allCards = getArguments().getBoolean(ARG_CARDS);
        System.out.println("init loader");
        getLoaderManager().initLoader(0, getArguments(), this);
        adapter = new CardAdapter(getActivity(), android.R.layout.simple_list_item_1, cards);
        for(int i=0;i<filterAdapter.getCount();i++) {
            adapter.addFilter(filterAdapter.getItem(i));
        }
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        listView.setOnItemClickListener(this);
        //listView.setOnCreateContextMenuListener(this);
        View contextMenu = inflater.inflate(R.layout.add_deck_context_menu, null, false);
        this.registerForContextMenu(contextMenu);
        
        final ListView filterList = (ListView)rootView.findViewById(R.id.listViewFilter);
        filterList.setAdapter(filterAdapter);
        
        final Button addFilter = (Button)rootView.findViewById(R.id.addFilter);
        addFilter.setOnClickListener(this);
        
        return rootView;
    }
    
    public void refresh() {
        getLoaderManager().restartLoader(0, getArguments(), this);
    }

    @Override
    public Loader<List<Card>> onCreateLoader(int id, Bundle args) {
        Log.i("onCreateLoader", "loader created");
        return new CardLoader(getActivity(), allCards, dbAdapter);
    }

    @Override
    public void onLoadFinished(Loader<List<Card>> loader, List<Card> result) {
        Log.i("onLoadFinished", result.size() + "");
        adapter.clear();
        adapter.addAll(result);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<List<Card>> loader) {
        adapter.clear();
        adapter.notifyDataSetChanged();
    }

	@Override
	public boolean onItemLongClick(AdapterView<?> adapterView, View child, int pos,
			long id) {
		if(!child.isSelected()) {
			child.setSelected(true);
			child.setBackgroundResource(R.drawable.list_selected);
			mSelectedCards.add(adapter.getItem(pos));
			mSelectedViews.add(child);
	        getActivity().startActionMode(this);
	        return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		switch(item.getItemId()) {
		case R.id.addToCollection:
			dbAdapter.addAllCardsToCollection(mSelectedCards);
			Toast.makeText(this.getActivity(), (CharSequence)this.getString(R.string.toast_added_card), Toast.LENGTH_SHORT).show();
			mode.finish();
			return true;
		case R.id.addToDeck:
		    getView().showContextMenu();
			mode.finish();
			return true;
		case R.id.addFilter:
		    FilterFragment filterFragment = new FilterFragment();
		    getActivity().getFragmentManager()
            .beginTransaction()
            .replace(R.id.container,
                    filterFragment).addToBackStack(null).commit();
		    return true;
		}
		return false;
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		if(allCards) {
			inflater.inflate(R.menu.card_context_all, menu);
		} else {
			inflater.inflate(R.menu.card_context_collection, menu);
		}
        return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mSelectedCards.clear();
		for(View v : mSelectedViews) {
			v.setSelected(false);
			v.setBackgroundResource(0);
		}
		mSelectedViews.clear();
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View child, int pos, long id) {
		if(!mSelectedCards.isEmpty() && !child.isSelected()) {
			child.setSelected(true);
			child.setBackgroundResource(R.drawable.list_selected);
			mSelectedCards.add(adapter.getItem(pos));
			mSelectedViews.add(child);
		} else {
			CardFragment cardFragment = CardFragment.newInstance(adapter.getItem(pos), getArguments().getInt(ARG_SECTION_NUMBER));
            getActivity().getFragmentManager()
            .beginTransaction()
            .replace(R.id.container,
                    cardFragment).addToBackStack(null).commit();
            
		}
	}

    @Override
    public void onClick(View view) {
        FilterFragment filterFragment = FilterFragment.newInstance(this, getArguments().getInt(ARG_SECTION_NUMBER));
        getActivity().getFragmentManager()
        .beginTransaction()
        .replace(R.id.container,
                filterFragment).addToBackStack(null).commit();
    }
    
    public ArrayAdapter<CardFilter> getFilterAdapter() {
        return filterAdapter;
    }
    
    public void addFilter(CardFilter filter) {
        filterAdapter.add(filter);
        filterAdapter.notifyDataSetChanged();
        adapter.addFilter(filter);
        adapter.notifyDataSetChanged();
    }
    
}
