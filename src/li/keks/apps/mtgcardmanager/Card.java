package li.keks.apps.mtgcardmanager;

import java.io.Serializable;

import android.content.ContentValues;
import android.database.Cursor;

public class Card implements Comparable<Card>, Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8839229267832359383L;
	public static final String[] fields = new String[] {
	    "id", "multiverseId", "set", "layout", "name", "nameBack", "manaCost", "convertedManaCost", "colors",
	    "type", "superTypes", "types", "subTypes", "rarity", "text", "flavor", "artist", "number", "power", 
	    "toughness", "loyalty", "variations", "imageName", "watermark", "border", "hand", "life", "foreignNames",
	    "comment", "price", "deck", "status"
	};
	public int      id = -1;
    public final int      multiverseId;
    public final String   set;
    public final String   layout;
    public final String   name;
    public final String   nameBack;
    public final Mana[]   manaCost;
    public final double   convertedManaCost;
    public final Color[]  colors;
    public final String   type;
    public final String[] superTypes;
    public final String[] types;
    public final String[] subTypes;
    public final Rarity   rarity;
    public final String   text;
    public final String   flavor;
    public final String   artist;
    public final String   number;
    public final String   power;
    public final String   toughness;
    public final int      loyalty;
    public final int[]    variations;
    public final String   imageName;
    public final String   watermark;
    public final String   border;
    public final int      hand;
    public final int      life;
    public final String[] foreignNames;
    public String         comment;
    public double         price;
    public Deck           deck;
    public Status         status;

    public static Card fromCursor(final Cursor c) {
        Card card = null;
        try {
            card = new Card(c.getInt(0), // id
                c.getString(1), // set
                c.getString(2), // layout
                c.getString(3), // name
                c.getString(4), // nameBack
                DatabaseOpenHelper.getManaArray(c, 5), // manacost
                c.getDouble(6), // cmc
                DatabaseOpenHelper.getColorArray(c, 7), // colors
                c.getString(8), // type
                DatabaseOpenHelper.getStringArray(c, 9), // supertypes
                DatabaseOpenHelper.getStringArray(c, 10), // types
                DatabaseOpenHelper.getStringArray(c, 11), // suptypes
                Rarity.getRarity(c.getString(12)), // rarity
                c.getString(13), // text
                c.getString(14), // flavor
                c.getString(15), // artist
                c.getString(16), // number
                c.getString(17), // power
                c.getString(18), // toughness
                c.getInt(19), // loyalty
                DatabaseOpenHelper.getIntArray(c, 20), // variations
                c.getString(21), // image name
                c.getString(22), // watermark
                c.getString(23), // border
                c.getInt(24), // hand
                c.getInt(25), // life
                DatabaseOpenHelper.getStringArray(c, 26)); // foreign name
        } catch (RuntimeException e) {
            e.printStackTrace();
            /*
            System.out.println("id: " + c.getInt(0)
                    + "\nset: " + c.getString(1)
                    + "\nlayout: " + c.getString(2)
                    + "\nname: " + c.getString(3)
                    + "\nnameBack: " + c.getString(4)
                    + "\nmanacost: " + c.getString(5)
                    + "\ncmc: " + c.getDouble(6)
                    + "\ncolors: " + c.getString(7)
                    + "\ntype: " + c.getString(8)
                    + "\nsupertypes: " + c.getString(8)
                    + "\ntypes: " + c.getString(9)
                    + "\nsubtypes: " + c.getString(10)
                    + "\nrarity: " + c.getString(11)
                    + "\ntext: " + c.getString(12)
                    + "\nflavor: " + c.getString(13)
                    + "\nartist: " + c.getString(14)
                    + "\nnumber: " + c.getString(15)
                    + "\npower: " + c.getString(16)
                    + "\ntoughness: " + c.getString(17)
                    + "\nloyalty: " + c.getInt(18)
                    + "\nvariations: " + c.getString(19)
                    + "\nimage name: " + c.getString(20)
                    + "\nwatermark: " + c.getString(21)
                    + "\nborder: " + c.getString(22)
                    + "\nhand: " + c.getInt(23)
                    + "\nlive: " + c.getInt(24)
                    + "\nforeign names: " + c.getString(25));
                    */
        }
        return card;
    }

    public Card(final int multiverseId, final String set, final String layout,
            final String name, final String nameBack, final Mana[] manaCost,
            final double convertedManaCost, final Color[] colors,
            final String type, final String[] superTypes, final String[] types,
            final String[] subTypes, final Rarity rarity, final String text,
            final String flavor, final String artist, final String number,
            final String power, final String toughness, final int loyalty,
            final int[] variations, final String imageName,
            final String watermark, final String border, final int hand,
            final int life, final String[] foreignNames) {
        this.multiverseId = multiverseId;
        this.set = set;
        this.layout = layout;
        this.name = name;
        this.nameBack = nameBack;
        this.manaCost = manaCost;
        this.convertedManaCost = convertedManaCost;
        this.colors = colors;
        this.type = type;
        this.superTypes = superTypes;
        this.types = types;
        this.subTypes = subTypes;
        this.rarity = rarity;
        this.text = text;
        this.flavor = flavor;
        this.artist = artist;
        this.number = number;
        this.power = power;
        this.toughness = toughness;
        this.loyalty = loyalty;
        this.variations = variations;
        this.imageName = imageName;
        this.watermark = watermark;
        this.border = border;
        this.hand = hand;
        this.life = life;
        this.foreignNames = foreignNames;
        this.status = Status.OWNED;
        this.comment = "";
        this.price = 0;
        this.deck = null;
        this.id = -1;
    }
    
    @Override
    public String toString() {
    	int id;
    	if(this.id == -1) {
    		id = this.multiverseId;
    	} else {
    		id = this.id;
    	}
        if(this.nameBack != null) {
            return this.name + " / " + this.nameBack + " ( " + id + " )";
        }
        return this.name + " ( " + id + " )";
    }

    @Override
    public int compareTo(Card other) {
        if(name.compareTo(other.name) == 0) {
            if(nameBack != null && other.nameBack != null && nameBack.compareTo(other.nameBack) == 0) {
                if(id == other.id) {
                    return 0;
                } else {
                    return id < other.id ? -1 : 1;
                }
            } else {
                if(nameBack != null) {
                    if(other.nameBack != null) {
                        return nameBack.compareTo(other.nameBack);
                    } else {
                        return 1;
                    }
                } else {
                    if(other.nameBack != null) {
                        return -1;
                    } else {
                        return name.compareTo(other.name);
                    }
                }
            }
        } else {
            return name.compareTo(other.name);
        }
    }
    
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.COLUMN_ID, multiverseId);
        values.put(DatabaseOpenHelper.COLUMN_SET, set);
        values.put(DatabaseOpenHelper.COLUMN_LAYOUT, layout);
        values.put(DatabaseOpenHelper.COLUMN_NAME, name);
        values.put(DatabaseOpenHelper.COLUMN_NAME_BACK, nameBack);
        values.put(DatabaseOpenHelper.COLUMN_MANA_COST, DatabaseOpenHelper.convertManaArray(manaCost));
        values.put(DatabaseOpenHelper.COLUMN_CONVERTED_MANA_COST, convertedManaCost);
        values.put(DatabaseOpenHelper.COLUMN_COLORS, DatabaseOpenHelper.convertColorArray(colors));
        values.put(DatabaseOpenHelper.COLUMN_TYPE, type);
        values.put(DatabaseOpenHelper.COLUMN_SUPERTYPES, DatabaseOpenHelper.convertStringArray(superTypes));
        values.put(DatabaseOpenHelper.COLUMN_TYPES, DatabaseOpenHelper.convertStringArray(types));
        values.put(DatabaseOpenHelper.COLUMN_SUBTYPES, DatabaseOpenHelper.convertStringArray(subTypes));
        values.put(DatabaseOpenHelper.COLUMN_RARITY, rarity.toString());
        values.put(DatabaseOpenHelper.COLUMN_TEXT, text);
        values.put(DatabaseOpenHelper.COLUMN_FLAVOR, flavor);
        values.put(DatabaseOpenHelper.COLUMN_ARTIST, artist);
        values.put(DatabaseOpenHelper.COLUMN_NUMBER, number);
        values.put(DatabaseOpenHelper.COLUMN_POWER, power);
        values.put(DatabaseOpenHelper.COLUMN_TOUGHNESS, toughness);
        values.put(DatabaseOpenHelper.COLUMN_LOYALTY, loyalty);
        values.put(DatabaseOpenHelper.COLUMN_VARIATIONS, DatabaseOpenHelper.convertIntArray(variations));
        values.put(DatabaseOpenHelper.COLUMN_IMAGE_NAME, imageName);
        values.put(DatabaseOpenHelper.COLUMN_WATERMARK, watermark);
        values.put(DatabaseOpenHelper.COLUMN_BORDER, border);
        values.put(DatabaseOpenHelper.COLUMN_HAND, hand);
        values.put(DatabaseOpenHelper.COLUMN_LIFE, life);
        values.put(DatabaseOpenHelper.COLUMN_FOREIGN_NAME, DatabaseOpenHelper.convertStringArray(foreignNames));
        return values;
    }
}
