package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DeckListFragment extends Fragment implements OnItemClickListener,
OnItemLongClickListener, ActionMode.Callback {

    private ArrayAdapter<Deck>        mAdapter;
    private List<Deck> mSelectedDecks = new ArrayList<Deck>();
    private List<View> mSelectedViews = new ArrayList<View>();

    public static final String ARG_SECTION_NUMBER = "section_number";

    public static DeckListFragment newInstance(final int sectionNumber) {
        final DeckListFragment fragment = new DeckListFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_deck_list,
                container, false);
        final ListView listView = (ListView) rootView
                .findViewById(R.id.listView);
        final DatabaseAdapter dbAdapter = new DatabaseAdapter(getActivity());
        final List<Deck> decks = dbAdapter.getDecks();
        mAdapter = new ArrayAdapter<Deck>(getActivity(),
                android.R.layout.simple_list_item_1, decks);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        return rootView;
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, final View child,
            final int pos, final long id) {
        if(!mSelectedDecks.isEmpty() && !child.isSelected()) {
            child.setSelected(true);
            child.setBackgroundResource(R.drawable.list_selected);
            mSelectedDecks.add(mAdapter.getItem(pos));
            mSelectedViews.add(child);
        } else {
            DeckDetailFragment cardFragment = DeckDetailFragment.newInstance(mAdapter.getItem(pos), getArguments().getInt(ARG_SECTION_NUMBER));
            getActivity().getFragmentManager()
            .beginTransaction()
            .replace(R.id.container,
                    cardFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> adapterView,
            final View child, final int pos, final long id) {
        if(!child.isSelected()) {
            child.setSelected(true);
            child.setBackgroundResource(R.drawable.list_selected);
            mSelectedDecks.add(mAdapter.getItem(pos));
            mSelectedViews.add(child);
            getActivity().startActionMode(this);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onActionItemClicked(ActionMode arg0, MenuItem arg1) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean onCreateActionMode(ActionMode arg0, Menu arg1) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
        // TODO Auto-generated method stub
        return false;
    }

}
