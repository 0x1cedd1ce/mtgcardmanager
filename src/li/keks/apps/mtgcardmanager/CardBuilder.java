package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;
import java.util.List;

public class CardBuilder {
    public int           id;
    public String        set = null;
    public String        layout = null;
    public String        name = null;
    public String        nameBack = null;
    public List<Mana>    manaCost = new ArrayList<Mana>();
    public double           convertedManaCost = 0;
    public List<Color>   colors = new ArrayList<Color>();
    public String        type = null;
    public List<String>  superTypes = new ArrayList<String>();
    public List<String>  types = new ArrayList<String>();
    public List<String>  subTypes = new ArrayList<String>();
    public Rarity        rarity = null;
    public String        text = null;
    public String        flavor = null;
    public String        artist = null;
    public String        number = null;
    public String        power = null;
    public String        toughness = null;
    public int           loyalty = 0;
    public List<Integer> variations = new ArrayList<Integer>();
    public String        imageName = null;
    public String        watermark = null;
    public String        border = null;
    public int           hand = 0;
    public int           life = 0;
    public List<String>  foreignNames = new ArrayList<String>();

    public Card build() {
        return new Card(id, set, layout, name, nameBack,
                manaCost.toArray(new Mana[0]), convertedManaCost,
                colors.toArray(new Color[0]), type,
                superTypes.toArray(new String[0]), types.toArray(new String[0]),
                subTypes.toArray(new String[0]), rarity, text, flavor, artist,
                number, power, toughness, loyalty, getVariations(), imageName,
                watermark, border, hand, life,
                foreignNames.toArray(new String[0]));
    }

    private int[] getVariations() {
        final int[] vars = new int[variations.size()];
        for (int i = 0; i < vars.length; i++) {
            vars[i] = variations.get(i);
        }
        return vars;
    }
}
