package li.keks.apps.mtgcardmanager;


public enum Color {
    RED,
    BLUE,
    BLACK,
    WHITE,
    GREEN,
    NONE;

    public static Color getColor(final String color) {
        switch (color) {
        case "Red":
            return RED;
        case "Blue":
            return BLUE;
        case "Black":
            return BLACK;
        case "White":
            return WHITE;
        case "Green":
            return GREEN;
        case "None":
        default:
            return NONE;
        }
    }
    
    public String toString() {
        switch(this) {
        case BLACK:
            return "Black";
        case BLUE:
            return "Blue";
        case GREEN:
            return "Green";
        case NONE:
            return "None";
        case RED:
            return "Red";
        case WHITE:
            return "White";
        default:
            return "None";
        }
    }
}
