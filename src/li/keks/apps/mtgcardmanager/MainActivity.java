package li.keks.apps.mtgcardmanager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity implements
NavigationDrawerFragment.NavigationDrawerCallbacks {
    
    private enum SHOWING {
        ALL, COLLECTION, ADD, DECK
    }
    
    /**
     * Fragment managing the behaviors, interactions and presentation of the
     * navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in
     * {@link #restoreActionBar()}.
     */
    private CharSequence             mTitle;
    
    /**
     * 
     */
    private SHOWING status = SHOWING.COLLECTION;

    /**
     * 
     */
    private LibraryFragment libraryFragment;
    private DeckListFragment deckFragment;
    /**
     * 
     */
    private AddCardFragment addCardFragment;
    
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
                .findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onNavigationDrawerItemSelected(final int position) {
        // update the main content by replacing fragments
        final FragmentManager fragmentManager = getFragmentManager();
        switch (position) {
        case 0:
            status = SHOWING.COLLECTION;
            libraryFragment = LibraryFragment.newInstance(false, 0);
            fragmentManager
            .beginTransaction()
            .replace(R.id.container,
                    libraryFragment).commit();
            break;
        case 1:
            status = SHOWING.DECK;
            deckFragment = DeckListFragment.newInstance(1);
            fragmentManager.beginTransaction()
            .replace(R.id.container, deckFragment).commit();
            break;
        case 2:
            status = SHOWING.ALL;
            libraryFragment = LibraryFragment.newInstance(true, 2);
            fragmentManager
            .beginTransaction()
            .replace(R.id.container,
                    libraryFragment).commit();
            break;
        case 3:
            status = SHOWING.ADD;
            addCardFragment = AddCardFragment.newInstance();
            fragmentManager
            .beginTransaction()
            .replace(R.id.container,
                    addCardFragment).commit();
            break;
        }
        
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_refresh) {
            if(status == SHOWING.COLLECTION) {
                libraryFragment.refresh();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(final int number) {
        switch (number) {
        case 0:
            mTitle = getString(R.string.title_my_cards);
            break;
        case 1:
            mTitle = getString(R.string.title_my_decks);
            break;
        case 2:
            mTitle = getString(R.string.title_all_cards);
            break;
        case 3:
            mTitle = getString(R.string.title_add_card);
            break;
        }
    }

    public void restoreActionBar() {
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }
}
