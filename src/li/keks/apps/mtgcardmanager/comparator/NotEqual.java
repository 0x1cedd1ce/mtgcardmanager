package li.keks.apps.mtgcardmanager.comparator;

import java.util.Locale;

import li.keks.apps.mtgcardmanager.Color;
import li.keks.apps.mtgcardmanager.Deck;
import li.keks.apps.mtgcardmanager.Mana;
import li.keks.apps.mtgcardmanager.Rarity;
import li.keks.apps.mtgcardmanager.Status;

public class NotEqual implements Comparator {
    
    /**
     * 
     */
    private static final long serialVersionUID = -6863634731680670414L;

    @Override
    public String toString() {
        return "Not equal";
    }

    @Override
    public boolean compare(String left, String right) {
        return left.toLowerCase(Locale.getDefault()) != right.toLowerCase(Locale.getDefault());
    }

    @Override
    public boolean compare(int left, String right) {
        try {
            int iRight = Integer.valueOf(right);
            return iRight != left;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public boolean compare(double left, String right) {
        try {
            double dRight = Double.valueOf(right);
            return dRight != left;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public boolean compare(int[] left, String right) {
        return true;
    }

    @Override
    public boolean compare(String[] left, String right) {
        return true;
    }

    @Override
    public boolean compare(Color[] left, String right) {
        return true;
    }

    @Override
    public boolean compare(Mana[] left, String right) {
        return true;
    }

    @Override
    public boolean compare(Status left, String right) {
        try {
            Status eRight = Status.fromString(right);
            return eRight != left;
        } catch (Exception e) {
            return true;
        }
    }

    @Override
    public boolean compare(Deck left, String right) {
        return (left.name.toLowerCase(Locale.getDefault()) != right.toLowerCase(Locale.getDefault())) &&
                compare(left.id, right);
    }

    @Override
    public boolean compare(Rarity left, String right) {
        return left != Rarity.getRarity(right) && compare(left.ordinal(), right);
    }

    @Override
    public boolean compare(int left, int right) {
        return left != right;
    }

    @Override
    public boolean compare(int left, double right) {
        return compare((double) left, right);
    }

    @Override
    public boolean compare(double left, int right) {
        return compare(left, (double)right);
    }

    @Override
    public boolean compare(double left, double right) {
        return left != right;
    }

    @Override
    public boolean compare(int[] left, int right) {
        return true;
    }

    @Override
    public boolean compare(int[] left, double right) {
        return true;
    }

    @Override
    public boolean compare(Rarity left, int right) {
        return left.ordinal() != right;
    }

    @Override
    public boolean compare(Rarity left, double right) {
        return left.ordinal() != right;
    }

}
