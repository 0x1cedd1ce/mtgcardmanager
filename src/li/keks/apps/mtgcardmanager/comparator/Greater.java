package li.keks.apps.mtgcardmanager.comparator;

import java.util.Locale;

import li.keks.apps.mtgcardmanager.Color;
import li.keks.apps.mtgcardmanager.Deck;
import li.keks.apps.mtgcardmanager.Mana;
import li.keks.apps.mtgcardmanager.Rarity;
import li.keks.apps.mtgcardmanager.Status;

public class Greater implements Comparator {

    /**
     * 
     */
    private static final long serialVersionUID = 376528538499234158L;

    @Override
    public String toString() {
        return "Greater";
    }
    
    @Override
    public boolean compare(String left, String right) {
        return left.toLowerCase(Locale.getDefault()).compareTo(right.toLowerCase(Locale.getDefault())) > 0;
    }

    @Override
    public boolean compare(int left, String right) {
        try {
            int iRight = Integer.valueOf(right);
            return left > iRight;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public boolean compare(double left, String right) {
        try {
            double dRight = Integer.valueOf(right);
            return left > dRight;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public boolean compare(int[] left, String right) {
        return false;
    }

    @Override
    public boolean compare(String[] left, String right) {
        return false;
    }

    @Override
    public boolean compare(Color[] left, String right) {
        return false;
    }

    @Override
    public boolean compare(Mana[] left, String right) {
        return false;
    }

    @Override
    public boolean compare(Status left, String right) {
        return false;
    }

    @Override
    public boolean compare(Deck left, String right) {
        return false;
    }

    @Override
    public boolean compare(Rarity left, String right) {
        try {
            Rarity eRight = Rarity.getRarity(right);
            return left.compareTo(eRight) > 0;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean compare(int left, int right) {
        return left > right;
    }

    @Override
    public boolean compare(int left, double right) {
        return left > right;
    }

    @Override
    public boolean compare(double left, int right) {
        return left > right;
    }

    @Override
    public boolean compare(double left, double right) {
        return left > right;
    }

    @Override
    public boolean compare(int[] left, int right) {
        return false;
    }

    @Override
    public boolean compare(int[] left, double right) {
        return false;
    }

    @Override
    public boolean compare(Rarity left, int right) {
        return left.ordinal() > right;
    }

    @Override
    public boolean compare(Rarity left, double right) {
        return left.ordinal() > right;
    }

}
