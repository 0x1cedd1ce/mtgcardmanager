package li.keks.apps.mtgcardmanager.comparator;

import java.util.Locale;

import li.keks.apps.mtgcardmanager.Color;
import li.keks.apps.mtgcardmanager.Deck;
import li.keks.apps.mtgcardmanager.Mana;
import li.keks.apps.mtgcardmanager.Rarity;
import li.keks.apps.mtgcardmanager.Status;

public class ContainsNot implements Comparator {

    /**
     * 
     */
    private static final long serialVersionUID = 4166693141124666314L;

    @Override
    public String toString() {
        return "Contains Not";
    }
    
    @Override
    public boolean compare(String left, String right) {
        return !left.toLowerCase(Locale.getDefault()).contains(right.toLowerCase(Locale.getDefault()));
    }

    @Override
    public boolean compare(int left, String right) {
        return true;
    }

    @Override
    public boolean compare(double left, String right) {
        return true;
    }

    @Override
    public boolean compare(int[] left, String right) {
        try {
            int iRight = Integer.valueOf(right).intValue();
            return compare(left, iRight);
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public boolean compare(String[] left, String right) {
        for(String s : left) {
            if(s.toLowerCase(Locale.getDefault()) == right.toLowerCase(Locale.getDefault())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean compare(Color[] left, String right) {
        for(Color c : left) {
            if(c.name().toLowerCase(Locale.getDefault()) == right.toLowerCase(Locale.getDefault())) {
                return false;
            }
            if(c.toString().toLowerCase(Locale.getDefault()) == right.toLowerCase(Locale.getDefault())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean compare(Mana[] left, String right) {
        for(Mana m : left) {
            if(m.name().toLowerCase(Locale.getDefault()) == right.toLowerCase(Locale.getDefault())) {
                return false;
            }
            if(m.toString().toLowerCase(Locale.getDefault()) == right.toLowerCase(Locale.getDefault())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean compare(Status left, String right) {
        return true;
    }

    @Override
    public boolean compare(Deck left, String right) {
        return true;
    }

    @Override
    public boolean compare(Rarity left, String right) {
        return true;
    }

    @Override
    public boolean compare(int left, int right) {
        return true;
    }

    @Override
    public boolean compare(int left, double right) {
        return true;
    }

    @Override
    public boolean compare(double left, int right) {
        return true;
    }

    @Override
    public boolean compare(double left, double right) {
        return true;
    }

    @Override
    public boolean compare(int[] left, int right) {
        for(int i : left) {
            if(i == right) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean compare(int[] left, double right) {
        for(int i : left) {
            if(i == right) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean compare(Rarity left, int right) {
        return true;
    }

    @Override
    public boolean compare(Rarity left, double right) {
        return true;
    }

}
