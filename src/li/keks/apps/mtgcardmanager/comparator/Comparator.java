package li.keks.apps.mtgcardmanager.comparator;

import java.io.Serializable;

import li.keks.apps.mtgcardmanager.Color;
import li.keks.apps.mtgcardmanager.Deck;
import li.keks.apps.mtgcardmanager.Mana;
import li.keks.apps.mtgcardmanager.Rarity;
import li.keks.apps.mtgcardmanager.Status;

public interface Comparator extends Serializable {
    boolean compare(String left, String right);
    boolean compare(int left, String right);
    boolean compare(double left, String right);
    boolean compare(int[] left, String right);
    boolean compare(String[] left, String right);
    boolean compare(Color[] left, String right);
    boolean compare(Mana[] left, String right);
    boolean compare(Status left, String right);
    boolean compare(Deck left, String right);
    boolean compare(Rarity left, String right);
    boolean compare(int left, int right);
    boolean compare(int left, double right);
    boolean compare(double left, int right);
    boolean compare(double left, double right);
    boolean compare(int[] left, int right);
    boolean compare(int[] left, double right);
    boolean compare(Rarity left, int right);
    boolean compare(Rarity left, double right);
    String toString();
}