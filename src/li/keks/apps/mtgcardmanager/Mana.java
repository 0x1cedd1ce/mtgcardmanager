package li.keks.apps.mtgcardmanager;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public enum Mana {
    W("W"),
    U("U"),
    B("B"),
    R("R"),
    G("G"),
    S("S"),
    X("X"),
    Y("Y"),
    Z("Z"),
    NULL("0"),
    HALF("h"),
    HALFW("hw"),
    HALFU("hu"),
    HALFB("hb"),
    HALFR("hr"),
    HALFG("hg"),
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    TEN("10"),
    ELEVEN("11"),
    TWELVE("12"),
    THIRTEEN("13"),
    FOURTEEN("14"),
    FIFTEEN("15"),
    SIXTEEN("16"),
    SEVENTEEN("17"),
    EIGHTEEN("18"),
    NINETEEN("19"),
    TWENTY("20"),
    HUNDRED("100"),
    MILLION("1000000"),
    WU("W/U"),
    WB("W/B"),
    WR("W/R"),
    WG("W/G"),
    WS("W/S"),
    UW("U/W"),
    UB("U/B"),
    UR("U/R"),
    UG("U/G"),
    US("U/S"),
    BW("B/W"),
    BU("B/U"),
    BR("B/R"),
    BG("B/G"),
    BS("B/S"),
    RW("R/W"),
    RU("R/U"),
    RB("R/B"),
    RG("R/G"),
    RS("R/S"),
    GW("G/W"),
    GU("G/U"),
    GB("G/B"),
    GR("G/R"),
    GS("G/S"),
    SW("S/W"),
    SU("S/U"),
    SB("S/B"),
    SR("S/R"),
    SG("S/G"),
    PW("P/W"),
    PU("P/U"),
    PB("P/B"),
    PR("P/R"),
    PG("P/G"),
    PS("P/S"),
    P("P"),
    TWOW("2/W"),
    TWOU("2/U"),
    TWOB("2/B"),
    TWOR("2/R"),
    TWOG("2/G"),
    TWOS("2/S"),
    THREEW("3/W"),
    THREEU("3/U"),
    THREEB("3/B"),
    THREEG("3/G"),
    THREES("3/S"),
    FOURW("4/W"),
    FOURU("4/U"),
    FOURB("4/B"),
    FOURR("4/R"),
    FOURG("4/G"),
    FOURS("4/S");
    
    private static Map<String, Mana> manaMap = new HashMap<String, Mana>();
    static {
        for(Mana m : Mana.values()) {
            manaMap.put(m.toString(), m);
        }
    }
    private String name;
    private Mana(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
       return this.name;
    }
    
    public String toFileName() {
    	return this.name.replace('/', '_').toLowerCase(Locale.getDefault());
    }
    
    public static Mana getMana(String s) {
        s = s.replace("{", "").replace("}", "");
        if(manaMap.containsKey(s)) {
            return manaMap.get(s);
        } else {
            throw new RuntimeException("No such Mana: \"" + s + "\"!");
        }
    }
}
