package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public static final String   TABLE_CARDS                = "cards";
    public static final String   TABLE_COLLECTION           = "collection";
    public static final String   TABLE_DECKS                = "decks";
    public static final String   TABLE_SETS                 = "t_sets";
    public static final String   TABLE_NAMES                = "names";
    public static final String   COLUMN_ID                  = "_id";
    public static final String   COLUMN_MULTIVERSE_ID       = "universe_id";
    public static final String   COLUMN_CARD_ID             = "card_id";
    public static final String   COLUMN_DECK_ID             = "deck_id";
    public static final String   COLUMN_NAME                = "name";
    public static final String   COLUMN_CARDS               = "cards";
    public static final String   COLUMN_SET                 = "c_set";
    public static final String   COLUMN_CODE                = "code";
    public static final String   COLUMN_GATHERER_CODE       = "gathererCode";
    public static final String   COLUMN_OLD_CODE            = "oldCode";
    public static final String   COLUMN_BLOCK               = "block";
    public static final String   COLUMN_BORDER              = "border";
    public static final String   COLUMN_RELEASE_DATE        = "releaseDate";
    public static final String   COLUMN_TYPE                = "type";
    public static final String   COLUMN_LAYOUT              = "layout";
    public static final String   COLUMN_ARTIST              = "artist";
    public static final String   COLUMN_COLORS              = "colors";
    public static final String   COLUMN_NAME_BACK           = "name_back";
    public static final String   COLUMN_CONVERTED_MANA_COST = "cmc";
    public static final String   COLUMN_TEXT                = "text";
    public static final String   COLUMN_FLAVOR              = "flavor";
    public static final String   COLUMN_FOREIGN_NAME        = "foreign_name";
    public static final String   COLUMN_HAND                = "hand";
    public static final String   COLUMN_IMAGE_NAME          = "image";
    public static final String   COLUMN_LANGUAGE            = "lang";
    public static final String   COLUMN_LIFE                = "life";
    public static final String   COLUMN_LOYALTY             = "loyalty";
    public static final String   COLUMN_MANA_COST           = "mana_cost";
    public static final String   COLUMN_NUMBER              = "number";
    public static final String   COLUMN_POWER               = "power";
    public static final String   COLUMN_RARITY              = "rarity";
    public static final String   COLUMN_SUBTYPES            = "subtypes";
    public static final String   COLUMN_SUPERTYPES          = "supertypes";
    public static final String   COLUMN_TOUGHNESS           = "toughness";
    public static final String   COLUMN_TYPES               = "types";
    public static final String   COLUMN_VARIATIONS          = "variatoins";
    public static final String   COLUMN_WATERMARK           = "watermark";
    public static final String   COLUMN_COMMENT             = "comment";
    public static final String   COLUMN_PRICE               = "price";
    public static final String   COLUMN_STATUS              = "status";

    public static final String[] TABLE_CARDS_COLUMNS        = { COLUMN_ID,
        COLUMN_SET, COLUMN_LAYOUT, COLUMN_NAME, COLUMN_NAME_BACK,
        COLUMN_MANA_COST, COLUMN_CONVERTED_MANA_COST, COLUMN_COLORS,
        COLUMN_TYPE, COLUMN_SUPERTYPES, COLUMN_TYPES, COLUMN_SUBTYPES,
        COLUMN_RARITY, COLUMN_TEXT, COLUMN_FLAVOR, COLUMN_ARTIST,
        COLUMN_NUMBER, COLUMN_POWER, COLUMN_TOUGHNESS, COLUMN_LOYALTY,
        COLUMN_VARIATIONS, COLUMN_IMAGE_NAME, COLUMN_WATERMARK,
        COLUMN_BORDER, COLUMN_HAND, COLUMN_LIFE, COLUMN_FOREIGN_NAME};

    public static final String[] TABLE_COLLECTION_COLUMNS   = { COLUMN_ID,
        COLUMN_MULTIVERSE_ID, COLUMN_DECK_ID, COLUMN_COMMENT, COLUMN_PRICE, COLUMN_STATUS};

    public static final String[] TABLE_DECKS_COLUMNS        = { COLUMN_ID,
        COLUMN_NAME, COLUMN_CARDS                      };

    public static final String[] TABLE_SETS_COLUMNS         = { COLUMN_CODE,
        COLUMN_NAME, COLUMN_GATHERER_CODE, COLUMN_OLD_CODE,
        COLUMN_RELEASE_DATE, COLUMN_BORDER, COLUMN_TYPE, COLUMN_BLOCK };

    public static final String[] TABLE_NAMES_COLUMNS        = { COLUMN_ID,
        COLUMN_NAME, COLUMN_FOREIGN_NAME, COLUMN_LANGUAGE };

    private static final String  DATABASE_NAME              = "cards.db";
    private static final int     DATABASE_VERSION           = 1;

    private static final String  DATABASE_CREATE_CARDS      = "create table "
            + TABLE_CARDS
            + "("
            + COLUMN_ID + " integer primary key unique, "
            + COLUMN_SET + " text, "
            + COLUMN_LAYOUT + " text, "
            + COLUMN_NAME + " text, "
            + COLUMN_NAME_BACK + " text, "
            + COLUMN_MANA_COST + " text, "
            + COLUMN_CONVERTED_MANA_COST + " text, "
            + COLUMN_COLORS + " text, "
            + COLUMN_TYPE + " text, "
            + COLUMN_SUPERTYPES + " text, "
            + COLUMN_TYPES + " text, "
            + COLUMN_SUBTYPES + " text, "
            + COLUMN_RARITY + " text, "
            + COLUMN_TEXT + " text, "
            + COLUMN_FLAVOR + " text, "
            + COLUMN_ARTIST + " text, "
            + COLUMN_NUMBER + " text, "
            + COLUMN_POWER + " text, "
            + COLUMN_TOUGHNESS + " text, "
            + COLUMN_LOYALTY + " integer, "
            + COLUMN_VARIATIONS + " text, "
            + COLUMN_IMAGE_NAME + " text, "
            + COLUMN_WATERMARK + " text, "
            + COLUMN_BORDER + " text, "
            + COLUMN_HAND + " integer, "
            + COLUMN_LIFE + " integer, "
            + COLUMN_FOREIGN_NAME + " text, "
            + "foreign key(" + COLUMN_SET + ") references " + TABLE_SETS + "(" + COLUMN_CODE + ")"
            + ");";

    private static final String  DATABASE_CREATE_COLLECTION = "create table "
            + TABLE_COLLECTION
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_MULTIVERSE_ID + " integer, "
            + COLUMN_DECK_ID + " integer, "
            + COLUMN_COMMENT + " text, "
            + COLUMN_PRICE + " text, "
            + COLUMN_STATUS + " text, "
            + "foreign key("
            + COLUMN_MULTIVERSE_ID
            + ") references "
            + TABLE_CARDS
            + "("
            + COLUMN_ID
            + "), "
            + "foreign key("
            + COLUMN_DECK_ID
            + ") references "
            + TABLE_DECKS
            + "("
            + COLUMN_ID
            + ")"
            + ");";

    private static final String  DATABASE_CREATE_DECKS      = "create table "
            + TABLE_DECKS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text,"
            + COLUMN_CARDS + " text);";

    private static final String  DATABASE_CREATE_SETS       = "create table "
            + TABLE_SETS
            + "("
            + COLUMN_CODE + " text primary key, "
            + COLUMN_NAME + " text, "
            + COLUMN_GATHERER_CODE + " text, "
            + COLUMN_OLD_CODE + " text, "
            + COLUMN_RELEASE_DATE + " text, "
            + COLUMN_BORDER + " text, "
            + COLUMN_TYPE + " text, "
            + COLUMN_BLOCK + " text"
            + ");";

    private static final String  DATABASE_CREATE_NAMES      = " create table "
            + TABLE_NAMES
            + "("
            + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NAME
            + " text, "
            + COLUMN_FOREIGN_NAME
            + " text, "
            + COLUMN_LANGUAGE
            + " text, "
            + "foreign key("
            + COLUMN_NAME
            + ") references "
            + TABLE_CARDS
            + "("
            + COLUMN_NAME
            + ")"
            + ");";

    public static Color[] getColorArray(final Cursor c, final int column) {
        final String[] str_colors = getStringArray(c, column);
        final ArrayList<Color> colors = new ArrayList<Color>(str_colors.length);
        for (final String color : str_colors) {
            colors.add(Color.getColor(color.trim()));
        }
        return colors.toArray(new Color[0]);
    }
    
    public static String convertColorArray(Color[] array) {
        String ret = "";
        for(Color a : array) {
            ret += a + ",";
        }
        return ret;
    }

    public static int[] getIntArray(final Cursor c, final int column) {
        final String[] str_int = getStringArray(c, column);
        final int[] ints = new int[str_int.length];
        for (int i = 0; i < ints.length; i++) {
        	try {
        		ints[i] = Integer.valueOf(str_int[i]);
        	} catch (NumberFormatException e) {
        		Log.i("DatabaseOpenHelper", "Not a number: " + str_int[i]);
        	}
        }
        return ints;
    }
    
    public static String convertIntArray(int[] array) {
        String ret = "";
        for(int i : array) {
            ret += i + ",";
        }
        return ret;
    }

    public static Mana[] getManaArray(final Cursor c, final int column) {
        final String[] str_mana = getStringArray(c, column);
        final ArrayList<Mana> manas = new ArrayList<Mana>(str_mana.length);
        for (final String mana : str_mana) {
            try {
                manas.add(Mana.getMana(mana.trim()));
            } catch (RuntimeException e) {
                Log.i("DatabaseOpenHelper", mana + " " + (mana==null));
            }
        }
        return manas.toArray(new Mana[0]);
    }
    
    public static String convertManaArray(Mana[] array) {
        String ret = "";
        for(Mana m : array) {
            ret += m + ",";
        }
        return ret;
    }

    public static String[] getStringArray(final Cursor c, final int column) {
        String str = c.getString(column).trim();
        if(str.endsWith(",")) {
            str = str.substring(0, str.length()-1);
        }
        ArrayList<String> strs = new ArrayList<String>();
        for(String s : str.split(",")) {
        	if(s.trim() != "") {
        		strs.add(s.trim());
        	}
        }
        return strs.toArray(new String[0]);
    }
    
    public static String convertStringArray(String[] array) {
        String ret = "";
        for(String s : array) {
            ret += s + ",";
        }
        return ret;
    }

    public DatabaseOpenHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SETS);
        db.execSQL(DATABASE_CREATE_CARDS);
        db.execSQL(DATABASE_CREATE_DECKS);
        db.execSQL(DATABASE_CREATE_COLLECTION);
        db.execSQL(DATABASE_CREATE_NAMES);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion,
            final int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAMES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DECKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETS);
        onCreate(db);
    }

}
