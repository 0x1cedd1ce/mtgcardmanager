package li.keks.apps.mtgcardmanager;

import java.util.Locale;

public enum Status {
    OWNED, FOR_SALE, WANTED;
    
    public static Status fromString(String value) {
        String new_value = value.toUpperCase(Locale.getDefault()).replace(' ', '_');
        return Status.valueOf(new_value);
    }
}
