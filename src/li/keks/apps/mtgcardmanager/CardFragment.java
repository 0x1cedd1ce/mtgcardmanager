package li.keks.apps.mtgcardmanager;

import java.io.IOException;

import com.caverock.androidsvg.SVGImageView;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CardFragment extends Fragment {

	public static final String ARG_CARD = "card";
	public static final String ARG_SECTION_NUMBER = "section_number";
	
	private Card card;
	
	public static CardFragment newInstance(Card card, int sectionNumber) {
        final CardFragment fragment = new CardFragment();
        final Bundle args = new Bundle();
        args.putSerializable(ARG_CARD, card);
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_card,
                container, false);
        card = (Card)getArguments().getSerializable(ARG_CARD);
        TextView flavor = (TextView)rootView.findViewById(R.id.flavor);
        flavor.setText(card.flavor);
        TextView rules = (TextView)rootView.findViewById(R.id.rules);
        rules.setText(card.text);
        TextView cardName = (TextView)rootView.findViewById(R.id.cardName);
        cardName.setText(card.name);
        TextView artist = (TextView)rootView.findViewById(R.id.artistName);
        artist.setText(card.artist);
        TextView type = (TextView)rootView.findViewById(R.id.cardType);
        type.setText(card.type);
        LinearLayout manaCost = (LinearLayout)rootView.findViewById(R.id.manaCost);
        for(Mana m : card.manaCost) {
        	SVGImageView manaIcon = new SquareSVGImageView(getActivity());
        	String icon_name = "symbols/mana_" + m.toFileName() + ".svg";
        	try {
        		getActivity().getAssets().open(icon_name).close();
                manaIcon.setImageAsset(icon_name);
            } catch (IOException e) {
            	Log.i("CardFragment", "Mana icon not found: " + icon_name);
            }
        	manaCost.addView(manaIcon);
        }
        
        ImageView cardImage = (ImageView)rootView.findViewById(R.id.cardImage);
        SVGImageView setIcon = (SVGImageView)rootView.findViewById(R.id.setIcon);
        String icon_name = "symbols/" + card.set + "_" + card.rarity.toFileName() + ".svg";
        try {
        	getActivity().getAssets().open(icon_name).close();
        	setIcon.setImageAsset(icon_name);
        } catch (IOException e) {
        	Log.i("CardFragment", "Set icon not found: " + icon_name);
        }
        return rootView;
    }
	
}
