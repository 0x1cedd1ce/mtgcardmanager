package li.keks.apps.mtgcardmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import android.content.ContentValues;
import android.database.Cursor;

public class Deck implements List<Card>, Serializable {
    
    public interface DeckIterator extends ListIterator<Card> {
        public String getName();
        public int getDeckId();
    }
    
    private class DeckIteratorImpl implements DeckIterator {

        private final ListIterator<Card> mIterator;
        private final String mName;
        private final int mId;
        
        public DeckIteratorImpl(String name, int id) {
            mIterator = mCards.listIterator();
            mName = name;
            mId = id;
        }
        
        public DeckIteratorImpl(String name, int id, int location) {
            mIterator = mCards.listIterator(location);
            mName = name;
            mId = id;
        }
        
        @Override
        public void add(Card card) {
            mIterator.add(card);
        }

        @Override
        public boolean hasNext() {
            return mIterator.hasNext();
        }

        @Override
        public boolean hasPrevious() {
            return mIterator.hasPrevious();
        }

        @Override
        public Card next() {
            return mIterator.next();
        }

        @Override
        public int nextIndex() {
            return mIterator.nextIndex();
        }

        @Override
        public Card previous() {
            return mIterator.previous();
        }

        @Override
        public int previousIndex() {
            return mIterator.previousIndex();
        }

        @Override
        public void remove() {
            mIterator.remove();
        }

        @Override
        public void set(Card card) {
            mIterator.set(card);
        }
        
        public String getName() {
            return mName;
        }
        
        public int getDeckId() {
            return mId;
        }
    }
    
    /**
     * 
     */
    private static final long serialVersionUID = -6373624597086971056L;
    public final String     name;
    public final int         id;
    private final List<Card> mCards;

    public Deck(final int id, final String name, final List<Card> cards) {
        this.id = id;
        mCards = cards;
        this.name = name;
    }
    
    public Deck(Cursor cur, DatabaseAdapter dbAdapter) {
        this.id = cur.getInt(0);
        this.name = cur.getString(1);
        int[] ids = DatabaseOpenHelper.getIntArray(cur, 2);
        mCards = new ArrayList<Card>();
        for(int id : ids) {
            mCards.add(dbAdapter.getCardFromCollection(id));
        }
    }
    
    @Override
    public boolean add(final Card card) {
        if (card != null) {
            mCards.add(card);
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll(final Collection<? extends Card> cards) {
        for (final Card card : cards) {
            if (!add(card)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        mCards.clear();
    }

    @Override
    public boolean contains(final Object card) {
        return mCards.contains(card);
    }

    @Override
    public boolean containsAll(final Collection<?> cards) {
        for (final Object card : cards) {
            if (!contains(card)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isEmpty() {
        return mCards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return mCards.iterator();
    }

    @Override
    public boolean remove(final Object card) {
        return mCards.remove(card);
    }

    @Override
    public boolean removeAll(final Collection<?> cards) {
        return mCards.removeAll(cards);
    }

    @Override
    public boolean retainAll(final Collection<?> cards) {
        return mCards.retainAll(cards);
    }

    @Override
    public int size() {
        return mCards.size();
    }

    @Override
    public Object[] toArray() {
        return mCards.toArray();
    }

    @Override
    public <T> T[] toArray(final T[] arg0) {
        return mCards.toArray(arg0);
    }
    
    @Override
    public String toString() {
        int[] array = new int[mCards.size()];
        for(int i = 0;i < array.length;i++) {
            array[i] = mCards.get(i).id;
        }
        return DatabaseOpenHelper.convertIntArray(array);
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.COLUMN_NAME, name);
        values.put(DatabaseOpenHelper.COLUMN_CARDS, toString());
        return values;
    }

    @Override
    public void add(int pos, Card card) {
        mCards.add(pos, card);
    }

    @Override
    public boolean addAll(int pos, Collection<? extends Card> collection) {
        return mCards.addAll(pos, collection);
    }

    @Override
    public Card get(int pos) {
        return mCards.get(pos);
    }

    @Override
    public int indexOf(Object card) {
        return mCards.indexOf(card);
    }

    @Override
    public int lastIndexOf(Object card) {
        return mCards.lastIndexOf(card);
    }

    @Override
    public ListIterator<Card> listIterator() {
        return new DeckIteratorImpl(name, id);
    }

    @Override
    public ListIterator<Card> listIterator(int location) {
        return new DeckIteratorImpl(name, id, location);
    }

    @Override
    public Card remove(int location) {
        return mCards.remove(location);
    }

    @Override
    public Card set(int location, Card card) {
        return mCards.set(location, card);
    }

    @Override
    public List<Card> subList(int start, int end) {
        return mCards.subList(start, end);
    }
    
}
