package li.keks.apps.mtgcardmanager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipFile;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.JsonReader;
import android.util.Log;

public class JSONAdapter {
    private InputStream cardFile;
    private InputStream setCodeFile;
    private InputStream setListFile;
    private ZipFile     zipFile;

    public JSONAdapter(final Context context) {
        final String[] files = context.fileList();
        setCodeFile = null;
        setListFile = null;
        cardFile = null;
        for (final String file : files) {
            if (file.endsWith("AllSets-x.json.zip")) {
                try {
                    zipFile = new ZipFile(context.getFileStreamPath(file));
                    cardFile = zipFile.getInputStream(zipFile
                            .getEntry("AllStes-x.json"));
                } catch (final IOException e) {
                    cardFile = null;
                }
            } else if (file.endsWith("SetCodes.json")) {
                try {
                    setCodeFile = context.openFileInput(file);
                } catch (final FileNotFoundException e) {
                    setCodeFile = null;
                }
            } else if (file.endsWith("SetList.json")) {
                try {
                    setListFile = context.openFileInput(file);
                } catch (final FileNotFoundException e) {
                    setListFile = null;
                }
            }
        }
        final AssetManager assMan = context.getAssets();
        if (cardFile == null) {
            try {
                cardFile = assMan.open("AllSets-x.json");
            } catch (final IOException e) {
                throw new RuntimeException("This will never happen");
            }
        }
        if (setCodeFile == null) {
            try {
                setCodeFile = assMan.open("SetCodes.json");
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        if (setListFile == null) {
            try {
                setListFile = assMan.open("SetList.json");
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Card> parseCards() {
        final List<Card> cards = new ArrayList<Card>();
        InputStreamReader isr;
        try {
            isr = new InputStreamReader(cardFile, "UTF-8");
        } catch (final UnsupportedEncodingException e1) {
            e1.printStackTrace();
            return cards;
        }
        final JsonReader reader = new JsonReader(isr);
        try {
            reader.beginObject();
            while (reader.hasNext()) {
                final String set = reader.nextName().toLowerCase(Locale.getDefault());
                reader.beginObject();
                while (reader.hasNext()) {
                    final String name = reader.nextName();
                    if (name.equals("cards")) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            cards.add(readCard(reader, set));
                        }
                        reader.endArray();
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            }
        } catch (final IOException e) {
            e.printStackTrace();
            return cards;
        }
        Collections.sort(cards);
        return cards;
    }

    private Card readCard(final JsonReader reader, final String setName)
            throws IOException {
        reader.beginObject();
        final CardBuilder builder = new CardBuilder();
        builder.set = setName;
        while (reader.hasNext()) {
            final String name = reader.nextName();
            switch (name) {
            case "layout":
                builder.layout = reader.nextString();
                break;
            case "name":
                builder.name = reader.nextString();
                break;
            case "names":
                reader.beginArray();
                builder.name = reader.nextString();
                builder.nameBack = reader.nextString();
                if(reader.hasNext()) {
                    builder.name = builder.name + " " + builder.nameBack;
                    builder.nameBack = null;
                    while(reader.hasNext()) {
                        builder.name += reader.nextString();
                    }
                }
                reader.endArray();
                break;
            case "manaCost":
                builder.manaCost = parseManaList(reader.nextString());
                break;
            case "cmc":
                builder.convertedManaCost = reader.nextDouble();
                break;
            case "colors":
                reader.beginArray();
                builder.colors = new ArrayList<Color>();
                while (reader.hasNext()) {
                    builder.colors.add(Color.getColor(reader.nextString()));
                }
                reader.endArray();
                break;
            case "type":
                builder.type = reader.nextString();
                break;
            case "supertypes":
                reader.beginArray();
                builder.superTypes = new ArrayList<String>();
                while (reader.hasNext()) {
                    builder.superTypes.add(reader.nextString());
                }
                reader.endArray();
                break;
            case "types":
                reader.beginArray();
                builder.types = new ArrayList<String>();
                while (reader.hasNext()) {
                    builder.types.add(reader.nextString());
                }
                reader.endArray();
                break;
            case "subtypes":
                reader.beginArray();
                builder.subTypes = new ArrayList<String>();
                while (reader.hasNext()) {
                    builder.subTypes.add(reader.nextString());
                }
                reader.endArray();
                break;
            case "rarity":
                builder.rarity = Rarity.getRarity(reader.nextString());
                break;
            case "text":
                builder.text = reader.nextString();
                break;
            case "flavor":
                builder.flavor = reader.nextString();
                break;
            case "artist":
                builder.artist = reader.nextString();
                break;
            case "number":
                builder.number = reader.nextString();
                break;
            case "power":
                builder.power = reader.nextString();
                break;
            case "toughness":
                builder.toughness = reader.nextString();
                break;
            case "loyalty":
                builder.loyalty = reader.nextInt();
                break;
            case "multiverseid":
                builder.id = reader.nextInt();
                if(builder.id == 0) {
                	Log.i("JSONAdapter", builder.name + " " + builder.id);
                }
                break;
            case "variations":
                reader.beginArray();
                builder.variations = new ArrayList<Integer>();
                while (reader.hasNext()) {
                    builder.variations.add(reader.nextInt());
                }
                reader.endArray();
                break;
            case "imageName":
                builder.imageName = reader.nextString();
                break;
            case "watermark":
                builder.watermark = reader.nextString();
                break;
            case "border":
                builder.border = reader.nextString();
                break;
            case "hand":
                builder.hand = reader.nextInt();
                break;
            case "life":
                builder.hand = reader.nextInt();
                break;
            case "rulings":
                reader.skipValue();
                break;
            case "foreignNames":
                reader.beginArray();
                builder.foreignNames = new ArrayList<String>();
                while (reader.hasNext()) {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        final String foreignName = reader.nextName();
                        if (foreignName.equals("name")) {
                            builder.foreignNames.add(reader.nextString());
                        } else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();
                }
                reader.endArray();
                break;
            case "printings":
                reader.skipValue();
                break;
            case "originalText":
                reader.skipValue();
                break;
            case "originalType":
                reader.skipValue();
                break;
            case "legalities":
                reader.skipValue();
                break;
            default:
                Log.i("JSONAdapter", name);
            }
        }
        reader.endObject();
        return builder.build();
    }
    
    public String export(List<Card> cards) {
        String json = "";
        return json;
    }
    
    private List<Mana> parseManaList(String manaString) {
        List<Mana> manas = new ArrayList<Mana>();
        for(String m : manaString.split("\\}\\{")) {
            manas.add(Mana.getMana(m));
        }
        return manas;
    }
}
