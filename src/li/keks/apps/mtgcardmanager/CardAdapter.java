package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

public class CardAdapter implements ListAdapter {
    private CardOrder cardOrder = CardOrder.NAME ;
    private List<CardFilter> cardFilter = new ArrayList<CardFilter>();
    private int mResourceId;
    private Context mContext;
    private List<Card> mCards;
    private List<Card> mFilteredCards = new ArrayList<Card>();
    private List<DataSetObserver> mObserver = new ArrayList<DataSetObserver>();
    
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView != null) {
			if(convertView.isSelected()) {
				convertView.setBackgroundResource(R.drawable.list_selected);
			} else {
				convertView.setBackgroundResource(0);
			}
			return convertView;
		} else {
		    LayoutInflater inflater = LayoutInflater.from(mContext);
			View v = inflater.inflate(mResourceId, null);
			TextView textView = (TextView)v.findViewById(android.R.id.text1);
			textView.setText(getItem(position).toString());
			return v;
		}
	}

	public CardAdapter(Context context, int resource, List<Card> objects) {
	    mContext = context;
	    mResourceId = resource;
	    mCards = objects;
	    applyFilter();
	}
	
	public void clearFilter() {
	    cardFilter.clear();
	    applyFilter();
	}
	
	public void addFilter(CardFilter filter) {
	    cardFilter.add(filter);
	    applyFilter();
	}
	
	public void removeFilter(CardFilter filter) {
	    if(cardFilter.contains(filter)) {
	        cardFilter.remove(filter);
	    }
	    applyFilter();
	}
	
	private void applyFilter() {
	    mFilteredCards.clear();
        for(Card c : mCards) {
            boolean isFiltered = false;
            for(CardFilter f : cardFilter) {
                if(!f.isFiltered(c)) {
                    isFiltered = true;
                }
            }
            if(!isFiltered) {
                mFilteredCards.add(c);
            }
        }
	    applyOrder();
	    notifyDataSetChanged();
	}
	
	private void applyOrder() {
	    cardOrder.sort(mFilteredCards);
	    notifyDataSetChanged();
	}

    @Override
    public int getCount() {
        return mFilteredCards.size();
    }

    @Override
    public Card getItem(int pos) {
        return mFilteredCards.get(pos);
    }

    public void setOrder(CardOrder order) {
        cardOrder = order;
        applyOrder();
    }

    @Override
    public long getItemId(int pos) {
        return mFilteredCards.get(pos).multiverseId;
    }

    @Override
    public int getItemViewType(int pos) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return mFilteredCards.isEmpty();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if(!mObserver.contains(observer))
        {
            mObserver.add(observer);
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if(mObserver.contains(observer)) {
            mObserver.remove(observer);
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int pos) {
        return true;
    }

    public void clear() {
        mFilteredCards.clear();
        mCards.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Card> cards) {
        mCards.addAll(cards);
        applyFilter();
    }

    public void notifyDataSetChanged() {
        for(DataSetObserver o : mObserver) {
            o.onChanged();
        }
    }
    
    public void notifyDataSetInvalidated() {
        for(DataSetObserver o : mObserver) {
            o.onInvalidated();
        }
    }

}
