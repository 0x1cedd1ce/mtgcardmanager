package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DeckDetailFragment extends Fragment implements
OnItemClickListener, OnItemLongClickListener, ActionMode.Callback {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save) {
            DatabaseAdapter dbAdapter = new DatabaseAdapter(getActivity());
            if(dbAdapter.updateDeck(mDeck)) {
                mModified = false;
                Toast.makeText(getActivity(), "Deck saved.", Toast.LENGTH_SHORT)
                .show();
            } else {
                Toast.makeText(getActivity(), "Failed to save Deck.", Toast.LENGTH_LONG)
                .show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(mModified) {
            inflater.inflate(R.menu.deck_detail, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private ArrayAdapter<Card> mAdapter;
    private Deck mDeck;
    private ArrayList<Card> mSelectedCards = new ArrayList<Card>();
    private ArrayList<View> mSelectedViews = new ArrayList<View>();
    private boolean mModified = false;
    
    public static final String ARG_SECTION_NUMBER = "section_number";
    public static final String ARG_DECK = "deck";

    public static DeckDetailFragment newInstance(final Deck deck, final int sectionNumber) {
        final DeckDetailFragment fragment = new DeckDetailFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_DECK, deck);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_deck_list,
                container, false);
        final ListView listView = (ListView) rootView
                .findViewById(R.id.listView);
        mDeck = (Deck)getArguments().getSerializable(ARG_DECK);
        mAdapter = new ArrayAdapter<Card>(getActivity(),
                android.R.layout.simple_list_item_1, mDeck);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        return rootView;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View child, int pos,
            long id) {
        if(!child.isSelected()) {
            child.setSelected(true);
            child.setBackgroundResource(R.drawable.list_selected);
            mSelectedCards.add(mAdapter.getItem(pos));
            mSelectedViews.add(child);
            getActivity().startActionMode(this);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
        case R.id.removeFromDeck:
            mDeck.removeAll(mSelectedCards);
            mModified = true;
            Toast.makeText(this.getActivity(), (CharSequence)this.getString(R.string.toast_added_card), Toast.LENGTH_SHORT).show();
            mode.finish();
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.deck_detail_context, menu);
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mSelectedCards.clear();
        for(View v : mSelectedViews) {
            v.setSelected(false);
            v.setBackgroundResource(0);
        }
        mSelectedViews.clear();
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View child, int pos, long id) {
        if(!mSelectedCards.isEmpty() && !child.isSelected()) {
            child.setSelected(true);
            child.setBackgroundResource(R.drawable.list_selected);
            mSelectedCards.add(mAdapter.getItem(pos));
            mSelectedViews.add(child);
        } else {
            CardFragment cardFragment = CardFragment.newInstance(mAdapter.getItem(pos), getArguments().getInt(ARG_SECTION_NUMBER));
            getActivity().getFragmentManager()
            .beginTransaction()
            .replace(R.id.container,
                    cardFragment).addToBackStack(null).commit();
        }
    }
    
}
