package li.keks.apps.mtgcardmanager;

public enum Rarity {
    BASIC_LAND,
    COMMON,
    UNCOMMON,
    RARE,
    MYTHIC_RARE,
    SPECIAL;

    public static Rarity getRarity(final String s) {
        switch (s) {
        case "Common":
            return COMMON;
        case "Uncommon":
            return UNCOMMON;
        case "Rare":
            return RARE;
        case "Mythic Rare":
            return MYTHIC_RARE;
        case "Special":
            return SPECIAL;
        case "Basic Land":
            return BASIC_LAND;
        default:
            throw new RuntimeException("Unknown rarity: " + s + " " + (s == ""));
        }
    }
    
    @Override
    public String toString() {
        switch(this) {
        case BASIC_LAND:
            return "Basic Land";
        case COMMON:
            return "Common";
        case MYTHIC_RARE:
            return "Mythic Rare";
        case RARE:
            return "Rare";
        case SPECIAL:
            return "Special";
        case UNCOMMON:
            return "Uncommon";
        default:
            return "Common";
        }
    }
    
    public String toFileName() {
    	switch(this) {
    	case BASIC_LAND:
            return "common";
        case COMMON:
            return "common";
        case MYTHIC_RARE:
            return "mythic";
        case RARE:
            return "rare";
        case SPECIAL:
            return "special";
        case UNCOMMON:
            return "uncommon";
        default:
            return "common";
    	}
    }
}
