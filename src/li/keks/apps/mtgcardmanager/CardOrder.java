package li.keks.apps.mtgcardmanager;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum CardOrder {
    NAME,
    MANA,
    TOUGHNESS,
    POWER,
    COLOR,
    RARITY,
    ID,
    TYPE,
    PRICE,
    LIFE,
    HAND,
    LOYALTY;
    public void sort(List<Card> cards) {
        switch(this) {
        case COLOR:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    for(Color c : arg0.colors) {
                        for(Color c1 : arg1.colors) {
                            if(c == c1) {
                                continue;
                            }
                            return c.compareTo(c1);
                        }
                    }
                    return 0;
                }
            });
            break;
        case HAND:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.hand < arg1.hand) {
                        return -1;
                    } else if(arg0.hand > arg1.hand) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case ID:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.multiverseId < arg1.multiverseId) {
                        return -1;
                    } else if(arg0.multiverseId > arg1.multiverseId) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case LIFE:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.life < arg1.life) {
                        return -1;
                    } else if(arg0.life > arg1.life) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case LOYALTY:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.loyalty < arg1.loyalty) {
                        return -1;
                    } else if(arg0.loyalty > arg1.loyalty) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case MANA:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.convertedManaCost < arg1.convertedManaCost) {
                        return -1;
                    } else if(arg0.convertedManaCost > arg1.convertedManaCost) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case NAME:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    return arg0.name.compareTo(arg1.name);
                }
            });
            break;
        case POWER:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    return arg0.power.compareTo(arg1.power);
                }  
            });
            break;
        case PRICE:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    if(arg0.price < arg1.price) {
                        return -1;
                    } else if(arg0.price > arg1.price) {
                        return 1;
                    } else {
                        return 0;
                    }
                }  
            });
            break;
        case RARITY:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    return arg0.rarity.compareTo(arg1.rarity);
                }  
            });
            break;
        case TOUGHNESS:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    return arg0.toughness.compareTo(arg1.toughness);
                }
            });
            break;
        case TYPE:
            Collections.sort(cards, new Comparator<Card>() {
                @Override
                public int compare(Card arg0, Card arg1) {
                    return arg0.type.compareTo(arg1.type);
                }  
            });
            break;
        default:
            break;
        }
    }
}
