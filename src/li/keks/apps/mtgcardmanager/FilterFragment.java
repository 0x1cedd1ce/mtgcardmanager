package li.keks.apps.mtgcardmanager;


import java.lang.reflect.Field;

import li.keks.apps.mtgcardmanager.comparator.Comparator;
import li.keks.apps.mtgcardmanager.comparator.Contains;
import li.keks.apps.mtgcardmanager.comparator.ContainsNot;
import li.keks.apps.mtgcardmanager.comparator.Equal;
import li.keks.apps.mtgcardmanager.comparator.Greater;
import li.keks.apps.mtgcardmanager.comparator.GreaterOrEqual;
import li.keks.apps.mtgcardmanager.comparator.NotEqual;
import li.keks.apps.mtgcardmanager.comparator.Smaller;
import li.keks.apps.mtgcardmanager.comparator.SmallerOrEqual;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class FilterFragment extends Fragment implements OnClickListener {
    
    public static final String ARG_SECTION_NUMBER = "section";
    public static final String ARG_CARD_FILTER = "filter";
    private static final Comparator[] comparators = new Comparator[] {
      new Equal(), new NotEqual(), new Greater(), new GreaterOrEqual(), new Smaller(), 
      new SmallerOrEqual(), new Contains(), new ContainsNot()
    };
    
    private ArrayAdapter<CardFilter> mFilterAdapter;
    private Spinner mComparatorSelector;
    private Spinner mFieldSelector;
    private EditText mFilterParameter;
    private LibraryFragment mLibraryFragment;
    
    public static FilterFragment newInstance(LibraryFragment libraryFragment, int sectionNumber) {
        final FilterFragment fragment = new FilterFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        fragment.setLibraryFragment(libraryFragment);
        return fragment;
    }

    public void setLibraryFragment(LibraryFragment fragment) {
        mLibraryFragment = fragment;
    }
    
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_filter, container, false);
        ListView appliedFilters = (ListView)rootView.findViewById(R.id.appliedFilters);
        mFilterAdapter = new ArrayAdapter<CardFilter>(getActivity(), R.layout.filter_item, R.id.text1);
        for(int i=0;i<mLibraryFragment.getFilterAdapter().getCount();i++) {
            mFilterAdapter.add(mLibraryFragment.getFilterAdapter().getItem(i));
        }
        appliedFilters.setAdapter(mFilterAdapter);
        
        Button addFilter = (Button)rootView.findViewById(R.id.addFilter);
        addFilter.setOnClickListener(this);
        
        mComparatorSelector = (Spinner) rootView.findViewById(R.id.comparatorSelector);
        SpinnerAdapter comparatorAdapter = new ArrayAdapter<Comparator>(getActivity(), android.R.layout.simple_list_item_1, comparators);
        mComparatorSelector.setAdapter(comparatorAdapter);
        
        mFieldSelector = (Spinner)rootView.findViewById(R.id.fieldSelector);
        SpinnerAdapter fieldAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, Card.fields);
        mFieldSelector.setAdapter(fieldAdapter);
        
        mFilterParameter = (EditText)rootView.findViewById(R.id.filterParameter);
        
        return rootView;
    }
    
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.addFilter) {
            try {
                CardFilter filter = null;
                final Comparator comp = (Comparator)mComparatorSelector.getSelectedItem();
                final Field field = Card.class.getDeclaredField((String)mFieldSelector.getSelectedItem());
                final String filterParameter = mFilterParameter.getText().toString();
                if(field.getType() == int.class) {
                    try {
                        final int intField = Integer.valueOf(filterParameter).intValue();
                        filter = new CardFilter() {
                            private static final long serialVersionUID = -7019920255672279125L;
                            @Override
                            public boolean isFiltered(Card c) {
                                try {
                                    return comp.compare(field.getInt(c), intField);
                                } catch (IllegalAccessException
                                        | IllegalArgumentException e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                            @Override
                            public String toString() {
                                return field.getName() + " " + comp.toString() + " " + filterParameter;
                            }
                        };
                    } catch (NumberFormatException e) {
                        try {
                            final double doubleField = Double.valueOf(filterParameter).doubleValue();
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -4758510335731464201L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare(field.getInt(c), doubleField);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        } catch (NumberFormatException e1) {
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -8820196163339297040L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare(field.getInt(c), filterParameter);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        }
                    }
                } else if(field.getType() == double.class) {
                    try {
                        final int intField = Integer.valueOf(filterParameter).intValue();
                        filter = new CardFilter() {
                            private static final long serialVersionUID = 7355455731211215571L;
                            @Override
                            public boolean isFiltered(Card c) {
                                try {
                                    return comp.compare(field.getDouble(c), intField);
                                } catch (IllegalAccessException
                                        | IllegalArgumentException e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                            @Override
                            public String toString() {
                                return field.getName() + " " + comp.toString() + " " + filterParameter;
                            }
                        };
                    } catch (NumberFormatException e) {
                        try {
                            final double doubleField = Double.valueOf(filterParameter).doubleValue();
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -4454642208557405010L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare(field.getDouble(c), doubleField);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        } catch (NumberFormatException e1) {
                            filter = new CardFilter() {
                                private static final long serialVersionUID = 5979324756148819890L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare(field.getDouble(c), filterParameter);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        }
                    }
                } else if(field.getType() == String.class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = 7643559164050812341L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((String)field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                } else if(field.getType() == Deck.class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = 7624941397752226719L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((Deck)field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                } else if(field.getType() == Status.class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = 246738684433992917L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((Status)field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                } else if(field.getType() == Rarity.class) {
                    try {
                        final int intField = Integer.valueOf(filterParameter).intValue();
                        filter = new CardFilter() {
                            private static final long serialVersionUID = -783005191311808916L;
                            @Override
                            public boolean isFiltered(Card c) {
                                try {
                                    return comp.compare((Rarity)field.get(c), intField);
                                } catch (IllegalAccessException
                                        | IllegalArgumentException e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                            @Override
                            public String toString() {
                                return field.getName() + " " + comp.toString() + " " + filterParameter;
                            }
                        };
                    } catch (NumberFormatException e) {
                        try {
                            final double doubleField = Double.valueOf(filterParameter).doubleValue();
                            filter = new CardFilter() {
                                private static final long serialVersionUID = 3647616398835555195L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare((Rarity)field.get(c), doubleField);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        } catch (NumberFormatException e1) {
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -8813555528821439175L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare((Rarity)field.get(c), filterParameter);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        }
                    }
                } else if(field.getType() == int[].class) {
                    try {
                        final int intField = Integer.valueOf(filterParameter).intValue();
                        filter = new CardFilter() {
                            private static final long serialVersionUID = -7259949338918145910L;
                            @Override
                            public boolean isFiltered(Card c) {
                                try {
                                    return comp.compare((int[])field.get(c), intField);
                                } catch (IllegalAccessException
                                        | IllegalArgumentException e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                            @Override
                            public String toString() {
                                return field.getName() + " " + comp.toString() + " " + filterParameter;
                            }
                        };
                    } catch (NumberFormatException e) {
                        try {
                            final double doubleField = Double.valueOf(filterParameter).doubleValue();
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -5851641713365309299L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare((int[])field.get(c), doubleField);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        } catch (NumberFormatException e1) {
                            filter = new CardFilter() {
                                private static final long serialVersionUID = -952021692570715180L;
                                @Override
                                public boolean isFiltered(Card c) {
                                    try {
                                        return comp.compare((int[])field.get(c), filterParameter);
                                    } catch (IllegalAccessException
                                            | IllegalArgumentException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                                @Override
                                public String toString() {
                                    return field.getName() + " " + comp.toString() + " " + filterParameter;
                                }
                            };
                        }
                    }
                } else if(field.getType() == String[].class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = 7909213699936855620L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((String[])field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                } else if(field.getType() == Color[].class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = 4574975654572599254L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((Color[])field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                } else if(field.getType() == Mana[].class) {
                    filter = new CardFilter() {
                        private static final long serialVersionUID = -6312842869091740504L;
                        @Override
                        public boolean isFiltered(Card c) {
                            try {
                                return comp.compare((Mana[])field.get(c), filterParameter);
                            } catch (IllegalAccessException
                                    | IllegalArgumentException e) {
                                e.printStackTrace();
                                return false;
                            }
                        }
                        @Override
                        public String toString() {
                            return field.getName() + " " + comp.toString() + " " + filterParameter;
                        }
                    };
                }
                if(filter != null) {
                    mFilterAdapter.add(filter);
                    mLibraryFragment.addFilter(filter);
                    mFilterAdapter.notifyDataSetChanged();
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        } else {
            
        }
        
    }
}
