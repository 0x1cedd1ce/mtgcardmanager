package li.keks.apps.mtgcardmanager;

import java.io.Serializable;

public interface CardFilter extends Serializable {
    public boolean isFiltered(Card c);
    public String toString();
}
