package li.keks.apps.mtgcardmanager;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class DatabaseAdapter {
    
    private final SQLiteDatabase db;

    public DatabaseAdapter(final Context context) {
        final DatabaseOpenHelper helper = new DatabaseOpenHelper(context);
        db = helper.getWritableDatabase();
    }

    public boolean addCard(final Card card) {
    	try {
    		db.insertOrThrow(DatabaseOpenHelper.TABLE_CARDS, null, card.getContentValues());
    	} catch (SQLiteException e) {
    		Log.i("DatabaseAdapter", card.toString());
    	}
        return true;
    }
    
    public boolean addAllCards(final List<Card> cards) {
        for(Card c: cards) {
            addCard(c);
        }
        return true;
    }

    public boolean addDeck(final Deck deck) {
        db.insert(DatabaseOpenHelper.TABLE_DECKS, null, deck.getContentValues());
        return true;
    }
    
    public boolean addCardToCollection(final Card card) {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.COLUMN_COMMENT, card.comment);
        values.put(DatabaseOpenHelper.COLUMN_PRICE, card.price);
        values.put(DatabaseOpenHelper.COLUMN_STATUS, card.status.toString());
        values.put(DatabaseOpenHelper.COLUMN_MULTIVERSE_ID, card.multiverseId);
        if(card.deck != null) {
        	values.put(DatabaseOpenHelper.COLUMN_DECK_ID, card.deck.id);
        } else {
        	values.put(DatabaseOpenHelper.COLUMN_DECK_ID, -1);
        }
        db.insert(DatabaseOpenHelper.TABLE_COLLECTION, null, values);
        return true;
    }
    
    public boolean addAllCardsToCollection(final List<Card> cards) {
        for(Card c : cards) {
            addCardToCollection(c);
        }
        return true;
    }

    public List<Card> getAllCards() {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_CARDS,
                DatabaseOpenHelper.TABLE_CARDS_COLUMNS, null, null, null, null,
                null);
        return parseSQL(cur);
    }
    
    public List<Card> getAllCardsFromCollection() {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_COLLECTION,
                DatabaseOpenHelper.TABLE_COLLECTION_COLUMNS, null, null, null, null, null);
        cur.moveToFirst();
        List<Card> cards = new ArrayList<Card>();
        while(!cur.isAfterLast()) {
            cards.add(getCardFromCollection(cur.getInt(0)));
            cur.moveToNext();
        }
        return cards;
    }

    public List<Card> getCard(final String name) {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_CARDS, 
                DatabaseOpenHelper.TABLE_CARDS_COLUMNS, 
                DatabaseOpenHelper.COLUMN_NAME + "=? or " + DatabaseOpenHelper.COLUMN_NAME_BACK + "=?", 
                new String[]{name,  name}, null, null, null);
        return parseSQL(cur);
    }
    
    public Card getCard(final int id) {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_CARDS, 
                DatabaseOpenHelper.TABLE_CARDS_COLUMNS, 
                DatabaseOpenHelper.COLUMN_ID + "=?", 
                new String[]{id+""}, null, null, null);
        return parseSQL(cur).get(0);
    }
    
    public Card getCardFromCollection(final int id) {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_COLLECTION,
                DatabaseOpenHelper.TABLE_COLLECTION_COLUMNS,
                DatabaseOpenHelper.COLUMN_ID + "=?", new String[]{id+""}, null, null, null);
        cur.moveToFirst();
        final Card card = getCard(cur.getInt(1));
        card.id = cur.getInt(0);
        card.deck = getDeck(cur.getInt(2));
        card.comment = cur.getString(3);
        card.price = cur.getInt(4);
        card.status = Status.valueOf(cur.getString(5));
        return card;
    }

    public Deck getDeck(final String name) {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_DECKS, 
                DatabaseOpenHelper.TABLE_DECKS_COLUMNS, DatabaseOpenHelper.COLUMN_NAME+"=?", 
                new String[]{name}, null, null, null);
        cur.moveToFirst();
        return new Deck(cur, this);
    }
    
    public Deck getDeck(final int id) {
    	if(id == -1) {
    		return null;
    	}
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_DECKS, 
                DatabaseOpenHelper.TABLE_DECKS_COLUMNS, DatabaseOpenHelper.COLUMN_ID+"=?", 
                new String[]{id+""}, null, null, null);
        cur.moveToFirst();
        return new Deck(cur, this);
    }

    public List<Deck> getDecks() {
        final Cursor cur = db.query(DatabaseOpenHelper.TABLE_DECKS, 
                DatabaseOpenHelper.TABLE_DECKS_COLUMNS, null, null, null, null, null);
        cur.moveToFirst();
        List<Deck> decks = new ArrayList<Deck>();
        while(!cur.isAfterLast()) {
            decks.add(new Deck(cur, this));
            cur.moveToNext();
        }
        return decks;
    }

    private List<Card> parseSQL(final Cursor c) {
        final ArrayList<Card> cards = new ArrayList<Card>(c.getCount());
        if (!c.isFirst()) {
            c.moveToFirst();
        }
        while (!c.isAfterLast()) {
            cards.add(Card.fromCursor(c));
            c.moveToNext();
        }
        return cards;
    }

    public boolean removeCardFromCollection(final Card card) {
        final int amount = db.delete(DatabaseOpenHelper.TABLE_COLLECTION,
                DatabaseOpenHelper.COLUMN_ID + "=?", new String[]{card.id+""});
        return amount >= 1;
    }

    public boolean removeDeck(final Deck deck) {
        final int amount = db.delete(DatabaseOpenHelper.TABLE_DECKS, 
                DatabaseOpenHelper.COLUMN_ID, new String[]{deck.id+""});
        return amount >= 1;
    }

    public boolean updateCardFromCollection(final Card card) {
        ContentValues values = card.getContentValues();
        values.put(DatabaseOpenHelper.COLUMN_COMMENT, card.comment);
        values.put(DatabaseOpenHelper.COLUMN_PRICE, card.price);
        values.put(DatabaseOpenHelper.COLUMN_STATUS, card.status.toString());
        values.put(DatabaseOpenHelper.COLUMN_MULTIVERSE_ID, card.multiverseId);
        values.put(DatabaseOpenHelper.COLUMN_DECK_ID, card.deck.id);
        final int amount = db.update(DatabaseOpenHelper.TABLE_COLLECTION, values, 
                DatabaseOpenHelper.COLUMN_ID + "=?", new String[]{card.id+""});
        return amount >= 1;
    }

    public boolean updateDeck(final Deck deck) {
        final int amount = db.update(DatabaseOpenHelper.TABLE_DECKS, deck.getContentValues(),
                DatabaseOpenHelper.COLUMN_ID + "=?", new String[]{deck.id+""});
        return amount >= 1;
    }
}
